#ifndef GAME_CLASS_HPP
# define GAME_CLASS_HPP

# include "Rotor.class.hpp"
# include "Utils/Vector.class.hpp"
# include "Utils/Map.class.hpp"
# include "SpaceShip.class.hpp"
# include "Obstacle.class.hpp"
# include "SpaceObject.class.hpp"
# include "Gun.class.hpp"
# include "Display.class.hpp"
# include "Select.class.hpp"

class Player;

class Game : public Rotor
{
private:
	Utils::Map<Object*, unsigned int>	_object_frame;
	Utils::Map<Object*, unsigned int>	_object_start_x;
	Utils::Map<Gun*, clock_t>	_last_fire;
	Utils::Vector<SpaceObject*>			_objects;
	Player								*_player;
	Object								*_player_zone;
	Display 							*_dis;
	Select								_key;
	clock_t								_time_place_flotte;
	clock_t								_time_new_flotte;

	Game();
	Game(int frames);
	Game(const Game& g);
	Game& operator=(const Game& g);
	static Game 	_singleton;

	bool	_is_alive(const SpaceShip* obj);
	bool	_canShoot(Gun *gun);
	bool	_tu_te_demerde(SpaceObject *skip);
	void	_recalculate(SpaceObject *obj, bool player_missile = false);
	void	_recalculate_pos_player();
	void	_recalculate_pos();
	void	_check_alives();
	void	_checkEnemies();
	void	_checkPlayerShoots();
	void	_placeEnemies();
	void	_manageKey();
	void	_shootPlayer();
	int		_getFreeX();
	int		_getFreeY();

public:
	virtual ~Game();

	static inline Game*	instance() {
		return (&Game::_singleton);
	}

	inline Display*		getDisplay() {
		return (this->_dis);
	}

	void			setPlayer(const std::string& name);

	virtual void	onFrame();
	void			init(const std::string& player_name);
};

#endif

#ifndef OBSTACLE_CLASS_HPP
#define OBSTACLE_CLASS_HPP

#include <iostream>
#include "SpaceObject.class.hpp"

class Obstacle : public SpaceObject {

public:
	Obstacle(void);
	Obstacle(Obstacle const& src);
	~Obstacle(void);

	Obstacle& operator=(Obstacle const& rhs);
private:

};

#endif //!OBSTACLE_CLASS_HPP

#ifndef MOVEWINDOW_CLASS_HPP
#define MOVEWINDOW_CLASS_HPP

#include <iostream>
#include "Window.class.hpp"

class MoveWindow : public Window {

public:
	MoveWindow(void);
	MoveWindow(Window*);
	MoveWindow(MoveWindow const& src);
	~MoveWindow(void);

	virtual void		newWin(int, int, int, int, bool);
	void		move(int, int, bool);

	MoveWindow& operator=(MoveWindow const& rhs);

private:
		Window*			_parent;
};


#endif //!MOVEWINDOW_CLASS_HPP

#ifndef SPACESHIP_CLASS_HPP
#define SPACESHIP_CLASS_HPP

# include <iostream>
# include "SpaceObject.class.hpp"
# include "Gun.class.hpp"

class SpaceShip : public SpaceObject {

public:
	SpaceShip(void);
	SpaceShip(int life);

	SpaceShip(SpaceShip const& src);
	~SpaceShip(void);

	int	getLife(void) const;

	inline Gun *getGun() {
		return (this->_gun);
	}

	inline SpaceShip*	setGun(Gun *g) {
		this->_gun = g;
		return (this);
	}

	inline SpaceShip	*setLife(int life) {
		this->_life = life;
		return (this);
	}

	SpaceShip& operator=(SpaceShip const& rhs);

private:
	int _life;
	Gun	*_gun;

};

std::ostream& operator<<(std::ostream& o, SpaceShip const& i);

#endif //!SPACESHIP_CLASS_HPP

#ifndef PLAYER_CLASS_HPP
# define PLAYER_CLASS_HPP

# include <iostream>
# include <string>
# include "SpaceShip.class.hpp"


class Player: public SpaceShip {

public:
	Player();
	Player(Player const &src);
	Player(std::string name);
	~Player();

	std::string		getName()		const;
	unsigned int	getScore()		const;
	unsigned int	getMaxY()		const;

	void			setMaxY(unsigned int max_y);

	inline void 	setScore(int score) {
		this->_score = score;
	}

	inline int		getMaxScore() const {
		return (this->_max_score);
	}

	inline void		setMaxScore(int maxscore) {
		this->_max_score = maxscore;
	}

	void			operator--();
	void			operator++();

	Player&			operator=(Player const &rhs);

private:
	std::string		_name;
	unsigned int	_score;
	unsigned int	_max_y;
	unsigned int	_max_score;
};

std::ostream		&operator<<(std::ostream &o, Player const &i);

#endif //!PLAYER_CLASS_HPP

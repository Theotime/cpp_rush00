#ifndef OBJECT_CLASS_HPP
#define OBJECT_CLASS_HPP

# include <iostream>

class Object {

public:
	Object(void);
	Object(int x, int y, int x_max, int y_max);
	Object(Object const& src);
	virtual ~Object(void);

	int	getX(void) const;
	int	getY(void) const;
	int	getX_max(void) const;
	int	getY_max(void) const;

	inline Object	*setX(int x) {
		this->_x = x;
		return (this);
	}
	inline Object 	*setY(int y) {
		this->_y = y;
		return (this);
	}
	inline Object 	*setX_max(int xmax) {
		this->_x_max = xmax;
		return (this);
	}
	inline Object 	*setY_max(int max) {
		this->_y_max = max;
		return (this);
	}

	Object& operator=(Object const& rhs);

private:
	int	_x;
	int	_y;
	int	_x_max;
	int	_y_max;

};

std::ostream& operator<<(std::ostream& o, Object const& i);

#endif //!OBJECT_CLASS_HPP

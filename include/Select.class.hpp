#ifndef SELECT_CLASS_HPP
#define SELECT_CLASS_HPP

#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include <sys/types.h>
#include <unistd.h>
#include <termios.h>

class Select {

public:
	Select(void);
	Select(Select const& src);
	~Select(void);

	char		update() ;

	Select& operator=(Select const& rhs);

private:
	fd_set				_fds;
};

#endif //!SELECT_CLASS_HPP

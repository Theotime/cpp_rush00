#ifndef MISSILE_CLASS_HPP
#define MISSILE_CLASS_HPP

#include <iostream>
# include "SpaceObject.class.hpp"

class Gun;

class Missile : public SpaceObject {

public:
	Missile(Gun *parent);
	Missile(Missile const& src);
	~Missile(void);


	Missile& operator=(Missile const& rhs);

	Gun 	*getParent();

private:
	Missile();
	Gun	*_parent;
};


#endif //!MISSILE_CLASS_HPP

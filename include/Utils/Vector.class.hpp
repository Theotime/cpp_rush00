#ifndef VECTOR_CLASS_HPP
# define VECTOR_CLASS_HPP

# include <stdint.h>

namespace Utils
{
	template <class T>
	class Vector
	{
	private:
		T			*_values;
		uint32_t	_size;
		uint32_t	_real_size;

	public:
		Vector() :
			_values(new T[10]),
			_size(0),
			_real_size(10)
		{
		}
		~Vector() {
			if (this->_values)
				delete [] this->_values;
		}
		Vector(const Vector<T>& v) :
			_values(new T[v._real_size]),
			_size(v._size),
			_real_size(v._real_size)
		{
			for (uint32_t i = 0; i < v._real_size; ++i) {
				this->_values[i] = v._values[i];
			}
		}
		Vector<T>&	operator=(const Vector<T>& v) {
			delete [] this->_values;
			this->_values = new T[v._real_size];
			this->_size = v._size;
			this->_real_size = v._real_size;
			for (uint32_t i = 0; i < this->_real_size; ++i) {
				this->_values[i] = v._values[i];
			}
			return (*this);
		}

		void	push(T value) {
			if (this->_size < this->_real_size) {
				this->_values[this->_size] = value;
				++this->_size;
			} else {
				this->_real_size *= 2;
				T	*values = new T[this->_real_size];
				uint32_t i = 0;
				for (; i < this->_size; ++i) {
					values[i] = this->_values[i];
				}
				delete [] this->_values;
				this->_values = values;
				push(value);
			}
		}

		void	erase(T value) {
			for (uint32_t i = 0; i < this->_size; ++i) {
				if (this->_values[i] == value) {
					for (uint32_t j = i; j < this->_real_size - 1; ++j) {
						this->_values[j] = this->_values[j + 1];
					}
					--this->_size;
					break ;
				}
			}
		}

		inline T&			operator[](uint32_t index) {
			return (this->_values[index]);
		}

		inline const T		operator[](uint32_t index) const {
			return (this->_values[index]);
		}

		inline uint32_t		size() const {
			return (this->_size);
		}
	};
}

#endif

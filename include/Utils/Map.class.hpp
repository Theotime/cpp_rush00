#ifndef MAP_CLASS_HPP
# define MAP_CLASS_HPP

# include "Utils/Vector.class.hpp"

namespace Utils
{
	template <class T, class V>
	class Map
	{
	private:
		struct map_item {
			T	first;
			V 	second;
			bool operator==(const map_item& item) {
				return (first == item.first);
			}
		};
		Utils::Vector<map_item>	_values;
	public:
		Map() {

		}
		~Map() {

		}

		Map(const Map<T, V>& m) :
			_values(m._values)
		{

		}

		void		erase(T key) {
			for (uint32_t i = 0; i < this->_values.size(); ++i) {
				if (this->_values[i].first == key) {
					this->_values.erase(this->_values[i]);
					break ;
				}
			}
		}

		bool		exist(T key) {
			for (uint32_t i = 0; i < this->_values.size(); ++i) {
				if (this->_values[i].first == key) {
					return (true);
				}
			}
			return (false);
		}

		Map<T, V>&	operator=(const Map<T, V>& m) {
			this->_values = m._values;
			return (*this);
		}

		V&			operator[](T key) {
			for (uint32_t i = 0; i < this->_values.size(); ++i) {
				if (this->_values[i].first == key) {
					return (this->_values[i].second);
				}
			}
			map_item m;
			m.first = key;
			this->_values.push(m);
			return (this->_values[this->_values.size() - 1].second);
		}

		const V	operator[](T key) const {
			for (uint32_t i = 0; i < this->_values.size(); ++i) {
				if (this->_values[i].first == key) {
					return (this->_values[i].second);
				}
			}
			map_item m;
			m.first = key;
			return (m.second);
		}

		inline uint32_t	size() const {
			return (this->_values.size());
		}
	};
}

#endif
#ifndef COLLISION_CLASS_HPP
# define COLLISION_CLASS_HPP

# include "Object.class.hpp"

class Collision
{
private:
	Collision();
	Collision(const Collision& c);
	Collision& operator=(const Collision& c);
public:
	~Collision();
	static bool	has(const Object& o1, const Object& o2);
};

#endif
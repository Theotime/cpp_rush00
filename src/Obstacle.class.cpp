#include "Obstacle.class.hpp"

Obstacle::Obstacle() : SpaceObject() {
	return;
}

Obstacle::Obstacle(Obstacle const& src) : SpaceObject() {
	(void)src;
	return;
}

Obstacle::~Obstacle() {
	return;
}

Obstacle&	Obstacle::operator=(Obstacle const& rhs){
	Obstacle::operator=(rhs);
	return *this;
}

#include "Ncurses.class.hpp"
#include <ncurses.h>

Ncurses::Ncurses() {
	::initscr();
	return;
}

Ncurses::Ncurses(Ncurses const& src) {
	*this = src;
	return;
}

Ncurses::~Ncurses() {
	::endwin();
	return;
}

void	Ncurses::Draw(std::string str){
	::printw((char *) str.c_str());
	::getch();
	::endwin();
	return;
}

void	Ncurses::Refresh(void){
	::refresh();
	return;
}

Ncurses&	Ncurses::operator=(Ncurses const& rhs){
	if (this != &rhs) {
	}
	return *this;
}

#include "Gun.class.hpp"

Gun::Gun() : _speed(0), _power(0), _fire_speed(0) {
	
}

Gun::Gun(Gun const& src) : _speed(src._speed), _power(src._power), _fire_speed(src._fire_speed) {
	
}

Gun::~Gun() {
	for (uint32_t i = 0; i < this->_missiles.size(); ++i) {
		delete this->_missiles[i];
	}
}

Utils::Vector<Missile*>&	Gun::getMissiles(void) { return this->_missiles; }

Gun&	Gun::operator=(Gun const& rhs){
	this->_missiles = rhs._missiles;
	this->_speed = rhs._speed;
	this->_power = rhs._power;
	this->_fire_speed = rhs._fire_speed;
	return *this;
}

Missile 	*Gun::newMissile(int x, int y, int xmax, int ymax)
{
	Missile *m = new Missile(this);
	m->setSpeed(this->_speed);
	m->setPower(this->_power);
	m->setX(x - 2);
	m->setY(y);
	m->setX_max(xmax - 1);
	m->setY_max(ymax);
	(void)xmax;
	(void)ymax;
	this->_missiles.push(m);
	return (m);
}
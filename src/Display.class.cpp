#include <Display.class.hpp>
#include "MoveWindow.class.hpp"
#include "Missile.class.hpp"
#include <fstream>

Display::Display()  {this->_init();}

Display::Display(Display const& s)  {(void)s; this->_init();}

Display::~Display() {
	::endwin();
}

void		Display::_initNcurses() {
//	this->_nonBlock(1);
	::initscr();
	::nodelay(stdscr, true);
	::noecho();
	::curs_set(FALSE);
	::wrefresh(stdscr);
}

void		Display::_nonBlock(int state) const {
	struct termios ttystate;
	tcgetattr(STDIN_FILENO, &ttystate);
	if (state == 1) {
	ttystate.c_lflag &= ~ICANON;
	ttystate.c_cc[VMIN] = 1;
	}
	else if (state == 0)
		ttystate.c_lflag |= ICANON;
	tcsetattr(STDIN_FILENO, TCSANOW, &ttystate);
}

void		Display::_init() {
	int			height = 0, width = 0, xbegin = 0, ybegin = 0;

	this->_initNcurses();
	getmaxyx(stdscr, height, width);
	xbegin = (width - XTOTAL) / 2;
	ybegin = (height - YTOTAL) / 2;
	this->_up.newWin(YAFF, XTOTAL, ybegin, xbegin, false);
	int current_y = ybegin  + YAFF;
	this->_map.newWin(YTOTAL - YAFF, XTOTAL, current_y , xbegin, false);
	current_y += YTOTAL - YAFF;
	this->_down.newWin(YAFF, XTOTAL, current_y, xbegin, false);
	::wrefresh(this->_down.getWin());
	this->refresh();
}

void		Display::refresh() {
	::wrefresh(this->_map.getWin());
	::wrefresh(this->_up.getWin());
}

void		Display::display(Object* obj) {
	int x = obj->getX(), y = obj->getY();
	int x2 = obj->getX_max(), y2 = obj->getY_max();
	Missile *m = dynamic_cast<Missile*>(obj);
	if (!this->_obj.exist(obj))
	{
		MoveWindow		*win = new MoveWindow(&this->_map);
		win->newWin(y2 - y, x2 - x, y, x, m != 0);
		this->_obj[obj] = win;
	}
	else
	{
		this->_obj[obj]->move(y, x, m != 0);
	}
}

void		Display::undisplay(Object* obj) {
	if (this->_obj.exist(obj)) {
		::wclear(this->_obj[obj]->getWin());
		::wrefresh(this->_obj[obj]->getWin());
		this->_obj.erase(obj);
		this->refresh();
	}
}

Display&	Display::operator=(Display const& rhs){
	if (this != &rhs) {
	}
	return *this;
}

std::ostream & operator<<(std::ostream & o, Display  const& i){
	(void)o ;
	(void)i;
	return (o);
}

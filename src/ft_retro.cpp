#include <Ncurses.class.hpp>
#include <Obstacle.class.hpp>
#include "Display.class.hpp"
#include "ncurses.h"
#include "MoveWindow.class.hpp"
#include "Object.class.hpp"
#include "Game.class.hpp"
#include <sstream>

void		nonBlock(int state) {
	struct termios ttystate;
	tcgetattr(STDIN_FILENO, &ttystate);
	if (state == 1) {
	ttystate.c_lflag &= ~ICANON;
	ttystate.c_cc[VMIN] = 1;
	}
	else if (state == 0)
		ttystate.c_lflag |= ICANON;
	tcsetattr(STDIN_FILENO, TCSANOW, &ttystate);
}


int	main(int ac, char **av) {
	//nonBlock(1);
	if (ac == 2) {
		Game::instance()->init(av[1]);
		Game::instance()->run();
	} else {
		std::cerr << "usage: " << av[0] << " <player_name>" << std::endl;
		return (1);
	}
	return (0);
}


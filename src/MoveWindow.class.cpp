#include "MoveWindow.class.hpp"
#include <fstream>

MoveWindow::MoveWindow() : Window(), _parent(NULL) {}
MoveWindow::MoveWindow(Window* p) : Window(), _parent(p) {}
MoveWindow::MoveWindow(MoveWindow const& src) : Window(src), _parent(src._parent) {}
MoveWindow::~MoveWindow() {
	if (this->_win) {
		::wclear(this->_win);
		::wrefresh(this->_win);
		::delwin(this->_win);
	}
}

void		MoveWindow::newWin(int height, int width, int y, int x, bool missile) {
	if (this->_parent) {
		this->_win = ::subwin(this->_parent->getWin(), height, width , this->_parent->getY() + y +1, this->_parent->getX() + x +1 );
		this->_x = x;
		this->_y = y;
		if (missile) {
			::mvwprintw(this->_win, y, x, "o");
		}
		else
			::wborder(this->_win, '.', '.', '.', '.', '.', '.', '.', '.');
		::wrefresh(this->_win);
	}
}

void		MoveWindow::move(int y, int x, bool missile) {
	if (this->_parent) {
		::wclear(this->_win);
		::wrefresh(this->_win);
		::mvwin(this->_win, this->_parent->getY() + y + 1, this->_parent->getX() + x + 1);
		if (missile) {
			::mvwprintw(this->_win, 0, 0, "<-");
			::mvwprintw(this->_win, 1, 0, "<-");
		}
		else
		{
			::wborder(this->_win, '.', '.', '.', '.', '.', '.', '.', '.');
		}
		::wrefresh(this->_win);
	}

}

MoveWindow&	MoveWindow::operator=(MoveWindow const& rhs){
	Window::operator=(rhs);
	return *this;
}

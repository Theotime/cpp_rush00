#include "Object.class.hpp"

Object::Object() : _x(0), _y(0), _x_max(0), _y_max(0){
	return;
}

Object::Object(int x, int y, int x_max, int y_max) :
	_x(x),
	_y(y),
	_x_max(x_max),
	_y_max(y_max)
{
	return;
}

Object::Object(Object const& src) {
	*this = src;
	return;
}

Object::~Object() {
	return;
}

int		Object::getX(void) const { return this->_x; }
int		Object::getY(void) const { return this->_y; }
int		Object::getX_max(void) const { return this->_x_max; }
int		Object::getY_max(void) const { return this->_y_max; }


Object&	Object::operator=(Object const& rhs){
	if (this != &rhs) {
		this->_x = getX(); 
		this->_y = getY(); 
		this->_x_max = getX_max(); 
		this->_y_max = getY_max(); 
	}
	return *this;
}

std::ostream & operator<<(std::ostream & o, Object const& i){
	o  << "X: " << i.getX() << std::endl
	   << "Y : " << i.getY() << std::endl
	   << "X Max :" << i.getX_max() << std::endl 
	   << "Y Max :" << i.getY_max() << std::endl;
	return (o);
}

#include "Player.class.hpp"

Player::Player():
	SpaceShip(),
	_score(0),
	_max_y(0)
{
	return;
}

Player::Player(Player const &src):
	SpaceShip(src),
	_score(0),
	_max_y(0)
{
	return;
}

Player::Player(std::string name):
	SpaceShip(250),
	_name(name),
	_score(0),
	_max_y(0)
{
}

Player::~Player() {
	return;
}

std::string		Player::getName()		const { return this->_name; }
unsigned int	Player::getScore()		const { return this->_score; }
unsigned int	Player::getMaxY()		const { return this->_max_y; }

void			Player::setMaxY(unsigned int max_y) {
	this->_max_y = max_y;
}

Player			&Player::operator=(Player const &rhs){
	if (this != &rhs) {
		this->_name = rhs.getName();
	}
	return *this;
}

void			Player::operator++() {
	if ((unsigned int)this->getY() < this->getMaxY() - 5) {
		this->setY(this->getY() + 1);
		this->setY_max(this->getY_max() + 1);
	}
}

void			Player::operator--() {
	if (this->getY() > 0) {
		this->setY(this->getY() - 1);
		this->setY_max(this->getY_max() - 1);
	}
}

std::ostream	&operator<<(std::ostream &o, Player const &i){
	o << i.getName();
	return o;
}

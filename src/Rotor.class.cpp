#include <Rotor.class.hpp>

Rotor::Rotor():
	_nb_frame(0),
	_end(false),
	_last_time_clock(0)
{
	this->setNbFramePerSec(25);
	return;
}

Rotor::Rotor(Rotor const& src) {
	*this = src;
	return;
}

Rotor::Rotor(int nb_frame_per_sec):
	_nb_frame(0),
	_end(false),
	_last_time_clock(0)
{
	this->setNbFramePerSec(nb_frame_per_sec);
}

Rotor::~Rotor() {
	return;
}

unsigned int	Rotor::getNbFramePerSec(void) const { return this->_nb_frame_per_sec; }
unsigned int	Rotor::getNbFrame()			const{ return this->_nb_frame; }
unsigned int	Rotor::getTimePerFrame()	const { return this->_time_per_frame; }
bool			Rotor::getEnd()				const { return this->_end; }

clock_t			Rotor::getTime(unsigned int number)			const {
	return (this->_time_per_frame * number);
}



Rotor			*Rotor::setNbFramePerSec(unsigned int nb_frame_per_sec) {
	if (!this->_nb_frame_per_sec || this->_nb_frame_per_sec != nb_frame_per_sec) {
		this->_time_per_frame = CLOCKS_PER_SEC / nb_frame_per_sec;
		this->_nb_frame_per_sec = nb_frame_per_sec;
	}
	return (this);
}

Rotor			*Rotor::setEnd(bool etas) {
	this->_end = etas;
	return this;
}

void			Rotor::onFrame() {
	
}

void			Rotor::run() {
	clock_t		start;
	clock_t		end;
	clock_t		t;

	while (!this->_end) {
		start = clock();
		this->onFrame();
		end = clock();
		t = end - start;
		this->_last_time_clock = t;
		++this->_nb_frame;
		//std::cout << *this;
		if (this->_time_per_frame - t > 0)
			usleep(this->_time_per_frame - t);
	}
}

Rotor&			Rotor::operator=(Rotor const& rhs){
	if (this != &rhs) {
		this->setNbFramePerSec(rhs.getNbFramePerSec());
	}
	return *this;
}

std::ostream	&operator<<(std::ostream &str, Rotor const& o){
	str << "Frame Number         : " << o.getNbFrame() << std::endl;
	str << "Frame number per sec : " << o.getNbFramePerSec() << std::endl;
	str << "Time                 : " << o.getTime(o.getNbFrame()) << std::endl;
	str << "Time per frame       : " << o.getTimePerFrame() << std::endl;
	str << "CLOCKS_PER_SEC       : " << CLOCKS_PER_SEC << std::endl;
	str << "#######################" << std::endl;
	return str;
}
